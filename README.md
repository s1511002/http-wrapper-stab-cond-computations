Small http wrapper around a [sage library for stability condition computations](https://github.com/benjaminschmidt/stability_conditions)
authored by [Ben Schmidt](https://sites.google.com/site/benjaminschmidtmath/home).
