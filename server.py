from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
from stability_conditions import *
from sage.rings.rational_field import QQ
import json

hostName = "0.0.0.0"
serverPort = 3001

def getWalls(chern_character):
    """
    chern_character is a list
    returns None if there are inf walls
    """
    try:
        results = tilt.walls_left(Element(chern_character),p(2))
    except:
        return None

    return [
      {
          "wall": {
              "centre": float(tilt_wall.s),
              "radius2": float(tilt_wall.p)
          },
          "destabilisers": list(map(str,destabilisers))
      }
      for tilt_wall, destabilisers
      in results.items()
    ]


class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            get_params = dict(qc.split("=") for qc in urlparse(self.path).query.split("&"))
            v = Element(list(map(QQ,get_params["v"].split(","))))
        except:
            v = Element([1,1,QQ(-3/2)])
        walls = getWalls(v)
        response = {
            "path": self.path,
            "chern": str(v),
            "beta_minus": float(tilt.beta_minus(v)),
            "beta_plus": float(tilt.beta_plus(v)),
            "walls": walls
        }

        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

        self.wfile.write(bytes(json.dumps(response), "utf-8"))

if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
